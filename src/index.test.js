import { renderApp } from './renderApp'
require('./index')

jest.mock('./renderApp', () => ({
  renderApp: jest.fn().mockName('RenderApp')
}))

describe('Index - Entrypoint', () => {
  it('should call renderApp', () => {
    expect(renderApp).toHaveBeenCalled()
  })
})
