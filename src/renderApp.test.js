import React from 'react'
import { render } from 'react-dom'
import { WrappedApp } from './components/ReduxWrapper'
import { renderApp } from './renderApp.js'

jest.mock('react-dom')

jest.mock('./components/ReduxWrapper', () => ({
  WrappedApp: jest
    .fn()
    .mockName('WrappedApp')
    .mockReturnValue('<div>SomeReactElement</div>')
}))

describe('Render app', () => {
  const originalDocument = document

  beforeEach(() => {
    Object.defineProperty(document, 'getElementById', {
      get: () => jest.fn().mockReturnValue('<div id="root" />')
    })
    renderApp()
  })

  afterEach(() => {
    document = originalDocument
  })

  test('It should call render with the wrapped App and the element with the id root', () => {
    expect(WrappedApp).toHaveBeenCalled()
    expect(render).toHaveBeenCalledWith(
      '<div>SomeReactElement</div>',
      '<div id="root" />'
    )
  })
})
