import {
  FETCH_START,
  FETCH_SUCCESS,
  FETCH_FAILURE,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'

export const ui = (
  state = {
    fetching: false,
    error: false,
    isBalanceNegative: false
  },
  { type, payload }
) => {
  switch (type) {
    case FETCH_START:
      return {
        ...state,
        fetching: true,
        error: false
      }
    case FETCH_SUCCESS:
      return {
        ...state,
        fetching: false,
        error: false,
        isBalanceNegative: payload.balance < 0
      }
    case FETCH_FAILURE:
      return { ...state, fetching: false, error: true }
    case INCREMENT_BALANCE:
      return {
        ...state,
        isBalanceNegative: payload.balance + payload.amount < 0
      }
    case DECREMENT_BALANCE:
      return {
        ...state,
        isBalanceNegative: payload.balance - payload.amount < 0
      }
    default:
      return state
  }
}
