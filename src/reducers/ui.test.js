import { ui } from './ui'
import {
  FETCH_START,
  FETCH_FAILURE,
  FETCH_SUCCESS,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'

describe('[REDUCER] - UI', () => {
  test('a fetch start action should set the fetching flag to true, error to false', () => {
    const action = {
      type: FETCH_START,
      payload: {}
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ fetching: true, error: false })
  })

  test('a fetch start should not alter the negative balance flag', () => {
    const action = { type: FETCH_START, payload: {} }
    const result = ui(
      { fetching: false, error: false, isBalanceNegative: true },
      action
    )
    expect(result).toMatchObject({ isBalanceNegative: true })
  })

  test('a fetch success action should set the fetching flag to false and error to false', () => {
    const action = {
      type: FETCH_SUCCESS,
      payload: { name: 'Big Shaq', balance: -10 }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ fetching: false, error: false })
  })

  test('a fetch success action should set the negative balance flag to true if the balance is < 0', () => {
    const action = {
      type: FETCH_SUCCESS,
      payload: { name: 'Big Shaq', balance: -10 }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: true })
  })

  test('a fetch success action should set the negative balance flag to false if the balance is > 0', () => {
    const action = {
      type: FETCH_SUCCESS,
      payload: { name: 'Big Shaq', balance: 10 }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('a fetch success action should set the negative balance flag to false if the balance is == 0', () => {
    const action = {
      type: FETCH_SUCCESS,
      payload: { name: 'Big Shaq', balance: 10 }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('a fetch failure action should set the fetching flag to false and error to true', () => {
    const action = {
      type: FETCH_FAILURE,
      payload: {}
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ fetching: false, error: true })
  })

  test('a fetch failure should not alter the negative balance flag', () => {
    const action = {
      type: FETCH_FAILURE,
      payload: {}
    }
    const result = ui(
      { fetching: true, error: false, isBalanceNegative: true },
      action
    )
    expect(result).toMatchObject({ isBalanceNegative: true })
  })

  test('an increment balance action should set the isNegativeBalance to true if the balance is < 0', () => {
    const action = {
      type: INCREMENT_BALANCE,
      payload: {
        amount: 10,
        balance: -100
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: true })
  })

  test('an increment balance action should set the isNegativeBalance to false if the balance > 0', () => {
    const action = {
      type: INCREMENT_BALANCE,
      payload: {
        amount: 10,
        balance: -5
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('an increment balance action should set the isNegativeBalance to false if the balance === 0', () => {
    const action = {
      type: INCREMENT_BALANCE,
      payload: {
        amount: 10,
        balance: -10
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('a decrement balance action should set the isNegativeBalance to true if the balance < 0', () => {
    const action = {
      type: DECREMENT_BALANCE,
      payload: {
        amount: 100,
        balance: 50
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: true })
  })

  test('a decrement balance action should set the isNegativeBalance to false if the balance > 0', () => {
    const action = {
      type: DECREMENT_BALANCE,
      payload: {
        amount: 10,
        balance: 100
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('a decrement balance action should set the isNegativeBalance to false if the balance === 0', () => {
    const action = {
      type: DECREMENT_BALANCE,
      payload: {
        amount: 10,
        balance: 10
      }
    }
    const result = ui(undefined, action)
    expect(result).toMatchObject({ isBalanceNegative: false })
  })

  test('the default case should return the default state', () => {
    const action = { type: 'SOME UNKNOWN ACTION', payload: {} }
    const result = ui(undefined, action)
    expect(result).toMatchObject({
      fetching: false,
      error: false,
      isBalanceNegative: false
    })
  })
})
