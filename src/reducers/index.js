import { combineReducers } from 'redux'
import { account } from './account'
import { ui } from './ui'
export default combineReducers({ account, ui })
