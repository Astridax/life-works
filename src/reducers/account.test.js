import { account } from './account'
import {
  FETCH_SUCCESS,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'

describe('[REDUCER] - Account', () => {
  test('if a fetch success action is passed it should create the state with the data from the payload', () => {
    const action = {
      type: FETCH_SUCCESS,
      payload: { name: 'Big Shaq', balance: 5000 }
    }
    const result = account(undefined, action)
    expect(result).toEqual({
      name: 'Big Shaq',
      balance: 5000
    })
  })

  test('if an increment balance action is passed it should increment the balance by the amount specified in the action', () => {
    const action = { type: INCREMENT_BALANCE, payload: { amount: 10 } }
    const result = account({ name: 'Big Shaq', balance: 5000 }, action)
    expect(result).toEqual({ name: 'Big Shaq', balance: 5010 })
  })

  test('the increment action should only modify the balance', () => {
    const action = { type: INCREMENT_BALANCE, payload: { amount: 10 } }
    const result = account(undefined, action)
    expect(result).toEqual({ name: '', balance: 10 })
  })

  test('if an decrement balance action is passed it should decrement the balance by the amount specified in the action', () => {
    const action = { type: DECREMENT_BALANCE, payload: { amount: 10 } }
    const result = account({ name: 'Big Shaq', balance: 5000 }, action)
    expect(result).toEqual({ name: 'Big Shaq', balance: 4990 })
  })

  test('the decrement action should only modify the balance', () => {
    const action = { type: DECREMENT_BALANCE, payload: { amount: 10 } }
    const result = account(undefined, action)
    expect(result).toEqual({ name: '', balance: -10 })
  })

  test('the default case should return a default state', () => {
    const action = { type: 'SOME UNKNOWN ACTION', payload: {} }
    const result = account(undefined, action)
    expect(result).toEqual({ name: '', balance: 0 })
  })
})
