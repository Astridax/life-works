import rootReducer from './index.js'
import { combineReducers } from 'redux'
import { account } from './account'
import { ui } from './ui'

jest.mock('redux', () => ({
  combineReducers: jest.fn()
}))

describe('Root Reducer', () => {
  test('It should combine all the other reducers', () => {
    expect(combineReducers).toHaveBeenCalledWith({ account, ui })
  })
})
