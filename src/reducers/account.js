import {
  FETCH_SUCCESS,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'

export const account = (
  state = {
    name: '',
    balance: 0
  },
  { type, payload }
) => {
  switch (type) {
    case FETCH_SUCCESS:
      return { ...state, name: payload.name, balance: payload.balance }
    case INCREMENT_BALANCE:
      return { ...state, balance: state.balance + payload.amount }
    case DECREMENT_BALANCE:
      return { ...state, balance: state.balance - payload.amount }
    default:
      return state
  }
}
