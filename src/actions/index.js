import {
  FETCH_START,
  FETCH_SUCCESS,
  FETCH_FAILURE,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'
import { INCREMENT_AMOUNT, DECREMENT_AMOUNT } from '../constants/misc'

export const fetchStart = () => ({
  type: FETCH_START,
  payload: {}
})

export const fetchSuccess = deserialisedJsonPayload => ({
  type: FETCH_SUCCESS,
  payload: deserialisedJsonPayload
})

export const fetchFailure = error => ({
  type: FETCH_FAILURE,
  payload: { error }
})

export const incrementBalance = balance => ({
  type: INCREMENT_BALANCE,
  payload: { balance, amount: INCREMENT_AMOUNT }
})

export const decrementBalance = balance => ({
  type: DECREMENT_BALANCE,
  payload: { balance, amount: DECREMENT_AMOUNT }
})

export const fetchAccount = () => {
  return async function(dispatch) {
    dispatch(fetchStart())
    try {
      const response = await fetch('https://codepen.io/andismith/pen/oqzxyP.js')
      const json = await response.json()
      dispatch(fetchSuccess(json))
    } catch (error) {
      dispatch(fetchFailure(error))
    }
  }
}
