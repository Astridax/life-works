import {
  fetchStart,
  fetchSuccess,
  fetchFailure,
  incrementBalance,
  decrementBalance,
  fetchAccount
} from './index'

import {
  FETCH_START,
  FETCH_SUCCESS,
  FETCH_FAILURE,
  INCREMENT_BALANCE,
  DECREMENT_BALANCE
} from '../constants/actionNames'

import { INCREMENT_AMOUNT, DECREMENT_AMOUNT } from '../constants/misc'

describe('[ACTION CREATORS]', () => {
  describe('', () => {
    test(`Fetch start action should have type of ${FETCH_START} and an empty payload`, () => {
      const action = fetchStart()
      expect(action).toEqual({ type: FETCH_START, payload: {} })
    })

    test(`Fetch success action should have type of ${FETCH_SUCCESS} and an payload containing the payload from the network call`, () => {
      const parsedJsonPayload = { name: 'Big Shaq', balance: 12 }
      const action = fetchSuccess(parsedJsonPayload)
      expect(action).toEqual({
        type: FETCH_SUCCESS,
        payload: {
          name: 'Big Shaq',
          balance: 12
        }
      })
    })

    test(`Fetch success action should have type of ${FETCH_FAILURE} and an payload containing the error object`, () => {
      const error = new Error()
      let action = fetchFailure(error)
      expect(action).toEqual({
        type: FETCH_FAILURE,
        payload: { error }
      })
    })

    test(`Increment Balance action should have type of ${INCREMENT_BALANCE} and an payload containing the increment amount (${INCREMENT_AMOUNT}) and balance`, () => {
      const action = incrementBalance(12)
      expect(action).toEqual({
        type: INCREMENT_BALANCE,
        payload: {
          amount: INCREMENT_AMOUNT,
          balance: 12
        }
      })
    })

    test(`Decrement Balance action should have type of ${DECREMENT_BALANCE} and an payload containing the increment amount (${DECREMENT_AMOUNT}) and balance`, () => {
      const action = decrementBalance(12)
      expect(action).toEqual({
        type: DECREMENT_BALANCE,
        payload: {
          amount: DECREMENT_AMOUNT,
          balance: 12
        }
      })
    })

    describe('Fetch account', () => {
      let action, dispatch, account

      beforeEach(() => {
        dispatch = jest.fn()
        account = { name: 'Big Shaq', balance: 12 }
        action = fetchAccount()
      })

      test('Fetch account should return a function', () => {
        expect(action).toBeInstanceOf(Function)
      })

      test('That function should return a promise', () => {
        expect(action(dispatch)).toBeInstanceOf(Promise)
      })

      test('In the happy path, the fetch start action should be dispatched followed by fetch success with a parsed JSON body', async () => {
        fetch.once(JSON.stringify(account))
        await action(dispatch)

        expect(dispatch).toHaveBeenCalledWith({
          type: FETCH_START,
          payload: {}
        })

        expect(dispatch).toHaveBeenCalledWith({
          type: FETCH_SUCCESS,
          payload: {
            name: 'Big Shaq',
            balance: 12
          }
        })
      })

      test('In the sad path, the fetch start action should be dispatched followed by fetch failure with an error', async () => {
        const error = new Error('NOPE')
        fetch.mockRejectOnce(error)
        await action(dispatch)

        expect(dispatch).toHaveBeenCalledWith({
          type: FETCH_START,
          payload: {}
        })

        expect(dispatch).toHaveBeenCalledWith({
          type: FETCH_FAILURE,
          payload: {
            error
          }
        })
      })
    })
  })
})
