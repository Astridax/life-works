import React from 'react'
import { shallow } from 'enzyme'
import { connect } from 'react-redux'
import { DecrementButton, mapStateToProps, mergeProps } from './index'

import styles from './styles.css'
import { bindActionCreators } from 'redux'
import { decrementBalance } from '../../actions'
import { DECREMENT_BALANCE } from '../../constants/actionNames'
import { DECREMENT_AMOUNT } from '../../constants/misc'

jest.mock('react-redux', () => ({
  connect: jest.fn().mockReturnValue(jest.fn())
}))

describe('[COMPONENT] - DecrementButton', () => {
  let renderedComponent
  const defaultProps = {
    balance: 50,
    decrementBalance: jest.fn()
  }
  const renderComponent = overrides =>
    shallow(<DecrementButton {...defaultProps} {...overrides} />)

  beforeEach(() => {
    renderedComponent = renderComponent()
  })

  test('it should contain a button containing a down arrow unicode character', () => {
    expect(
      renderedComponent.containsMatchingElement(
        <button className={styles.button}>&#x2B07;</button>
      )
    ).toBe(true)
  })

  test('the button should have an onClick prop that calls the decrement action', () => {
    renderedComponent.find('button').prop('onClick')()
    expect(defaultProps.decrementBalance).toHaveBeenCalled()
  })
})

describe('[REDUX] - DecrementButton', () => {
  test('mergeProps should return an object with wrapped actions and the props from state', () => {
    const balance = 50
    const dispatch = jest.fn().mockImplementation(action => {
      return action
    })
    const mergedProps = mergeProps({ balance }, { dispatch }, null)
    expect(mergedProps.balance).toEqual(balance)
    expect(mergedProps.decrementBalance).toBeTruthy()
    expect(mergedProps.decrementBalance()).toEqual({
      type: DECREMENT_BALANCE,
      payload: { balance, amount: DECREMENT_AMOUNT }
    })
  })

  test('mapStateToProps should return an object containing the balance', () => {
    const state = { account: { name: 'Big Shaq', balance: 50 }, ui: {} }
    const props = mapStateToProps(state)

    expect(props).toEqual({ balance: 50 })
  })
})
