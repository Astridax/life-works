import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { decrementBalance } from '../../actions'
import { connect } from 'react-redux'
import styles from './styles.css'

export const mapStateToProps = ({ account }) => ({ balance: account.balance })
export const mergeProps = (stateProps, { dispatch }, ownProps) => {
  return {
    ...stateProps,
    ...ownProps,
    ...bindActionCreators(
      { decrementBalance: () => decrementBalance(stateProps.balance) },
      dispatch
    )
  }
}

@connect(mapStateToProps, null, mergeProps)
export class DecrementButton extends Component {
  render() {
    return (
      <button
        className={styles.button}
        onClick={() => this.props.decrementBalance()}
      >
        &#x2B07;
      </button>
    )
  }
}
