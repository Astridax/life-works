import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.css'

export const mapStateToProps = ({ account }) => ({ balance: account.balance })

@connect(mapStateToProps)
export class BalanceDisplay extends Component {
  render() {
    const { balance } = this.props
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Balance</h1>
        <h1 className={styles.balanceDisplay}>£{balance}</h1>
      </div>
    )
  }
}
