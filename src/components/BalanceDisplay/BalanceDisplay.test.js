import React from 'react'
import { shallow } from 'enzyme'
import { connect } from 'react-redux'
import { BalanceDisplay, mapStateToProps } from './index'

import styles from './styles.css'

jest.mock('react-redux', () => ({
  connect: jest.fn().mockReturnValue(jest.fn())
}))

describe('[COMPONENT] - BalanceDisplay', () => {
  let renderedComponent

  const defaultProps = { balance: 123 }

  const renderComponent = overrides =>
    shallow(<BalanceDisplay {...defaultProps} {...overrides} />)

  beforeEach(() => {
    renderedComponent = renderComponent()
  })

  test('it should have a container div with a classname', () => {
    expect(renderedComponent.find('div').prop('className')).toEqual('container')
  })

  test('it should contain a div containing the world balance', () => {
    expect(
      renderedComponent.containsMatchingElement(
        <h1 className={styles.title}>Balance</h1>
      )
    ).toBe(true)
  })

  test('it should contain a div containing the balance', () => {
    expect(
      renderedComponent.containsMatchingElement(
        <h1 className={styles.balanceDisplay}>£{defaultProps.balance}</h1>
      )
    ).toBe(true)
  })

  test('it should call connect with mapStateToProps')
  /*
    Not really testable with jest alone due to the way decorators work.
    Essentially you need to mock mapStateToProps for this test only (doable),
    but it needs to be replaces before the module is even required.
    Rewire and rewiremock I think can do this.
    Alternatively wrap the connect call in a function and don't use decorators
  */
})

describe('[REDUX] - BalanceDisplay', () => {
  const state = {
    account: {
      name: "Big Shaq's Account",
      balance: 123
    },
    ui: {
      isBalanceNegative: false
    }
  }

  test('mapStateToProps should return an object containing the balance', () => {
    const result = mapStateToProps(state)
    expect(result).toEqual({
      balance: state.account.balance
    })
  })
})
