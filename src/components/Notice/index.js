import React, { Component } from 'react'
import { connect } from 'react-redux'
import styles from './styles.css'

export const mapStateToProps = ({ ui }) => {
  if (ui.error) return { mode: 'error' }
  if (ui.fetching) return { mode: 'fetching' }
  if (ui.isBalanceNegative && !(ui.error, ui.fetching))
    return { mode: 'negative_balance' }
  if (!(ui.isBalanceNegative && ui.error && ui.fetching))
    return { mode: 'disabled' }
}

@connect(mapStateToProps, null)
export class Notice extends Component {
  render() {
    let { mode } = this.props
    switch (mode) {
      case 'negative_balance':
        return <div className={styles.warning}>WARNING - Balance Negative</div>
      case 'fetching':
        return <div className={styles.info}>INFO - Fetching account</div>
      case 'error':
        return <div className={styles.error}>ERROR - Error fetching</div>
      case 'disabled':
      default:
        return null
    }
  }
}
