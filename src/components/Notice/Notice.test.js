import React from 'react'
import { shallow } from 'enzyme'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Notice, mapStateToProps, mapDispatchToProps } from './index'
import styles from './styles.css'

jest.mock('react-redux', () => ({
  connect: jest.fn().mockReturnValue(jest.fn())
}))

describe('[COMPONENT] - Notice', () => {
  let renderedComponent
  const defaultProps = {
    fetching: false,
    error: false,
    isBalanceNegative: false
  }
  const renderComponent = overrides =>
    shallow(<Notice {...defaultProps} {...overrides} />)

  beforeEach(() => {
    renderedComponent = renderComponent()
  })

  test("it should render null if the mode is 'disabled'", () => {
    expect(renderedComponent.getElement()).toBe(null)
  })

  test("it should render the 'negative balance' warning if the mode is 'negative_balance'", () => {
    expect(
      renderedComponent.containsMatchingElement(
        <div className={styles.warning}>"WARNING - Balance Negative"</div>
      )
    )
  })

  test("it should render the 'fetching account' info box if the mode is 'fetching'", () => {
    expect(
      renderedComponent.containsMatchingElement(
        <div className={styles.info}>INFO - Fetching account</div>
      )
    )
  })

  test("it should render the 'fetching account' error box if mode is 'error'", () => {
    expect(
      renderedComponent.containsMatchingElement(
        <div className={styles.error}>ERROR - Error fetching</div>
      )
    )
  })
})

//Should the logic controlling which Notice is visible go in the reducer?
describe('[REDUX] - Notice', () => {
  test("mapStateToProps should return an object with mode 'disabled' if ui state has all flags set to false", () => {
    const state = {
      account: {},
      ui: { isNegativeBalance: false, fetching: false, error: false }
    }
    const props = mapStateToProps(state)

    expect(props).toEqual({ mode: 'disabled' })
  })

  test("mapStateToProps should return an object with mode 'negative_balance' if ui state has isBalanceNegative set to true and error and fetching flags set to false", () => {
    const state = { account: {}, ui: { isBalanceNegative: true } }
    const props = mapStateToProps(state)

    expect(props).toEqual({ mode: 'negative_balance' })
  })

  test("mapStateToProps should return an object with mode 'fetching' if ui state has fetching set to true", () => {
    const state = { account: {}, ui: { fetching: true } }
    const props = mapStateToProps(state)

    expect(props).toEqual({ mode: 'fetching' })
  })

  test("mapStateToProps should return an object with mode 'error' if ui state has error set to true", () => {
    const state = { account: {}, ui: { error: true } }
    const props = mapStateToProps(state)

    expect(props).toEqual({ mode: 'error' })
  })
})
