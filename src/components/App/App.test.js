import React from 'react'
import { shallow } from 'enzyme'
import { connect } from 'react-redux'

import { App } from './index'
import { Notice } from '../Notice'
import { BalanceControlAndDisplay } from '../BalanceControlAndDisplay'
import { fetchAccount } from '../../actions'

jest.mock('../Notice', () => ({
  Notice: () => 'Notice'
}))

jest.mock('../BalanceControlAndDisplay', () => ({
  BalanceControlAndDisplay: () => 'Balance Control And Display'
}))

jest.mock('react-redux', () => ({
  connect: jest.fn().mockReturnValue(jest.fn())
}))

jest.mock('../../actions', () => ({
  fetchAccount: jest.fn()
}))

describe('[COMPONENT] - App', () => {
  let renderedApp
  let defaultProps = {
    fetchAccount
  }

  const renderApp = () => shallow(<App {...defaultProps} />)
  beforeEach(() => {
    renderedApp = renderApp()
  })

  test('the wrapper div should have a style', () => {
    expect(renderedApp.find('div').prop('className')).toEqual('container')
  })

  test('it should contain a Notice component', () => {
    expect(renderedApp.containsMatchingElement(<Notice />)).toBe(true)
  })

  test('it should contain a BalanceWidget', () => {
    expect(
      renderedApp.containsMatchingElement(<BalanceControlAndDisplay />)
    ).toBe(true)
  })
})

describe('[REDUX] - App', () => {
  test('connect should have been called with the fetchAccount action creator', () => {
    expect(connect).toHaveBeenCalledWith(null, { fetchAccount })
  })
})
