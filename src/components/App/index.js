import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Notice } from '../Notice'
import { BalanceControlAndDisplay } from '../BalanceControlAndDisplay'
import { fetchAccount } from '../../actions'
import styles from './styles.css'

@connect(null, { fetchAccount })
export class App extends Component {
  componentDidMount() {
    this.props.fetchAccount()
  }

  render() {
    return (
      <div className={styles.container}>
        <Notice />
        <BalanceControlAndDisplay />
      </div>
    )
  }
}
