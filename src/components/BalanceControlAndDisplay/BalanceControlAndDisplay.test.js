import React from 'react'
import { shallow } from 'enzyme'
import { BalanceControlAndDisplay } from '.'
import { BalanceDisplay } from '../BalanceDisplay'
import { IncrementButton } from '../IncrementButton'
import { DecrementButton } from '../DecrementButton'

jest.mock('../BalanceDisplay', () => ({
  BalanceDisplay: () => null
}))

jest.mock('../IncrementButton', () => ({
  IncrementButton: () => null
}))

jest.mock('../DecrementButton', () => ({
  DecrementButton: () => null
}))

describe('[COMPONENT] - BalanceControlAndDisplay', () => {
  let renderedComponent
  const renderComponent = () => shallow(<BalanceControlAndDisplay />)
  beforeEach(() => {
    renderedComponent = renderComponent()
  })

  test('it should contain a BalanceView component', () => {
    expect(renderedComponent.containsMatchingElement(<BalanceDisplay />)).toBe(
      true
    )
  })
  test('it should contain an IncrememntButton component', () => {
    expect(renderedComponent.containsMatchingElement(<IncrementButton />)).toBe(
      true
    )
  })
  test('it should contain a DecrementButton component', () => {
    expect(renderedComponent.containsMatchingElement(<DecrementButton />)).toBe(
      true
    )
  })
})
