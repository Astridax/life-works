import React from 'react'
import { BalanceDisplay } from '../BalanceDisplay'
import { IncrementButton } from '../IncrementButton'
import { DecrementButton } from '../DecrementButton'
export const BalanceControlAndDisplay = () => (
  <div>
    <BalanceDisplay />
    <IncrementButton />
    <DecrementButton />
  </div>
)
