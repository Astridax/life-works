import React from 'react'
import { shallow } from 'enzyme'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { incrementBalance } from '../../actions'
import { INCREMENT_BALANCE } from '../../constants/actionNames'
import { INCREMENT_AMOUNT } from '../../constants/misc'
import { IncrementButton, mapStateToProps, mergeProps } from './index'
import styles from './styles.css'

jest.mock('react-redux', () => ({
  connect: jest.fn().mockReturnValue(jest.fn())
}))

describe('[COMPONENT] - IncrementButton', () => {
  let renderedComponent
  const defaultProps = {
    balance: 50,
    incrementBalance: jest.fn()
  }
  const renderComponent = overrides =>
    shallow(<IncrementButton {...defaultProps} {...overrides} />)

  beforeEach(() => {
    renderedComponent = renderComponent()
  })

  test('it should contain a button containing a down arrow unicode character', () => {
    expect(
      renderedComponent.containsMatchingElement(
        <button className={styles.button}>&#x2B06;</button>
      )
    ).toBe(true)
  })

  test('the button should have an onClick prop that calls the increment action', () => {
    renderedComponent.find('button').prop('onClick')()
    expect(defaultProps.incrementBalance).toHaveBeenCalled()
  })
})

describe('[REDUX] - DecrementButton', () => {
  test('mapDispatchToProps should return an object with wrapped actions', () => {
    const balance = 50
    const dispatch = jest.fn().mockImplementation(action => {
      return action
    })
    const mergedProps = mergeProps({ balance }, { dispatch }, null)
    expect(mergedProps.balance).toEqual(balance)
    expect(mergedProps.incrementBalance).toBeTruthy()
    expect(mergedProps.incrementBalance()).toEqual({
      type: INCREMENT_BALANCE,
      payload: { balance, amount: INCREMENT_AMOUNT }
    })
  })

  test('mapStateToProps should return an object containing the balance', () => {
    const state = { account: { name: 'Big Shaq', balance: 50 }, ui: {} }
    const props = mapStateToProps(state)

    expect(props).toEqual({ balance: 50 })
  })
})
