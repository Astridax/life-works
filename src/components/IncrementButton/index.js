import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { incrementBalance } from '../../actions'
import { connect } from 'react-redux'
import styles from './styles.css'

export const mapStateToProps = ({ account }) => ({ balance: account.balance })
export const mergeProps = (stateProps, { dispatch }, ownProps) => {
  return {
    ...stateProps,
    ...ownProps,
    ...bindActionCreators(
      { incrementBalance: () => incrementBalance(stateProps.balance) },
      dispatch
    )
  }
}

@connect(mapStateToProps, null, mergeProps)
export class IncrementButton extends Component {
  render() {
    return (
      <button
        className={styles.button}
        onClick={() => this.props.incrementBalance()}
      >
        &#x2B06;
      </button>
    )
  }
}
