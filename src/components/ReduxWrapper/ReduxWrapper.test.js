import React from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { shallow } from 'enzyme'
import rootReducer from '../../reducers'
import { App } from '../App'
import { WrappedApp } from './index'

jest.mock('../../reducers', () => ({
  default: {}
}))

jest.mock('redux-thunk', () => jest.fn())

jest.mock('redux', () => ({
  createStore: jest.fn().mockReturnValue('store'),
  applyMiddleware: jest.fn().mockReturnValue('middlewear')
}))

jest.mock('react-redux', () => ({
  Provider: () => 'Provider'
}))

jest.mock('../App', () => ({
  App: () => 'App'
}))

describe('Wrapped App Component', () => {
  let renderedApp

  const renderApp = () => shallow(<WrappedApp />)

  beforeEach(() => {
    renderedApp = renderApp()
  })

  test('The store should be created with the root reducer', () => {
    expect(createStore).toHaveBeenCalledWith(rootReducer, 'middlewear')
  })

  test('It should have a Redux Provider with the store passed in and the app as the child', () => {
    expect(
      renderedApp.containsMatchingElement(
        <Provider store={'store'}>
          <App />
        </Provider>
      )
    ).toBe(true)
  })
})
