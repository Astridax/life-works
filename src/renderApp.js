import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { WrappedApp } from './components/ReduxWrapper'

export const renderApp = () => {
  render(WrappedApp(), document.getElementById('root'))
}
